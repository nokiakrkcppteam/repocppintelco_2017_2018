# Kontakt

Rozwiązania - link do repo studenta z rozwiązaniem - z numerem rewizji - z podaniem czy repo studenta wyszło od repo RepoCppInTelco_2017_2018 i od której rewizji (żeby można było zrobic diff na potrzeby review) 
proszę podesłać  na adres: 

* [C++ in Telecommunation Trainers](mailto:support.cpp_uj@nokia.com)

# DoD

Definition of Done dla zadań: 

* kod się kompiluje (proszę podać wersję użytego kompilatora)
* wszystkie testy przechodzą
* testów nie może być mniej niż w wersji bazowej (czyli z repo RepoCppInTelco_2017_2018)
* reszta warunków podana w czasie kursu (prezentacja z DoD w nazwie)

# Zadanie do zrobienie po kursie 

W nawiasach podane są nasze estymaty w godzinach.

## Obsługa szyfrowania w SMS-ach (20h)

Chodzi o te punkty specyfikacji:

* 3.3.2 Encryption
* 3.3.3 SMS message
* 4.1.3 Receiving SMS
* 4,1.5 Sending SMS


RSA encryption możemy potraktować jako zadanie dodatkow (+8h). 
Dla RSA trzeba użyć biblioteki OpenSSH (jest w projekcie - dostępna na uczelni).

Ekstra ocenione bedzie:

1. Minimalna ingerencja w istniejący kod - dobre odseparowanie kodu szyfrującego (np przy pomocy Strategy Design Patten)
1. Pokrycie testami
 
## Pokrycie testami klas służących interakcji z użytkownikiem (8h)

W obecnej wersji projektu nie ma testow do:

1. Klasy Users 
1. Klasy UserPort

Zadanie polega na dodaniu testów dla tych klas.

## Poprawa testów do klasy Timers (16h)

Testy do klasy timers:

1. Nie spełniają kryterium Fast(szybkie (patrz reguły F.I.R.S.T.) - tzn zastąpić użycie std::chrono::this_thread::wait_for czymś innym
1. Brakuje pokrycia testami  - tzn trzeba dodać to pokrycie

## Implementacja scenariuszy Dial/Talk (40h)

Chodzi o te punkty specyfikacji:

* 3.3.4 Call messages
* 4.1.6 Receiving Call Request
* 4.1.7 Sending Call Request
* 4.1.8 Dropping Call
* 4.1.9 Call (talking)

Z tym, że chodzi nie o rozmowę głosową - a o zwykły czat: https://pl.wikipedia.org/wiki/Czat

Ekstra ocenione bedzie:

1. Wpisanie się w obecny design (rozwiniecię maszyny stanów i portów - a nie implementowanie tego na "boku")
1. Pokrycie testami
1. Dodanie szyfrowania (patrz szyfrowanie SMS-ów) - to extra 16h

## Interkacje między scenariuszami (10h + 20h interakcje z Dial/Talk jeśli zadanie poprzednie też zrobione)

Chodzi o rozdział 4.2 specyfikacji.

Ekstra ocenione bedzie:

1. Wpisanie się w obecny design (implementacja na obecnej maszynie stanów - a nie implementowanie tego na "boku")
1. Pokrycie testami

