#pragma once

#include <vector>

class AA_const_cast
{
public:
  auto begin() const { return aa.begin(); }
  auto end() const { return aa.end(); }

  void push_back(int a) const { const_cast<std::vector<int>&>(aa).push_back(a); }

private:
  std::vector<int> aa;
};

