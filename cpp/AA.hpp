#pragma once

#include <vector>

class AA
{
public:
  auto begin() const { return aa.begin(); }
  auto end() const { return aa.end(); }

  void push_back(int a) { aa.push_back(a); }

private:
  std::vector<int> aa;
};

