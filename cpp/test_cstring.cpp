#include <iostream>
#include <string>
#include <cstring>
#include <typeinfo>
#include <memory>
#include <cxxabi.h>

template <typename T> void printCString(T&& str) {
#ifdef __GNUG__
    char* type = abi::__cxa_demangle(typeid(str).name(), 0, 0, 0);
    std::cout << type;
    std::free(type);
#else
    std::cout << typeid(str).name();
#endif
    std::cout << ',' << sizeof(str) << ',' << std::strlen(str) << ",\"" << str << "\"\n";
}

int main() {
    const char* localkPointerToStaticCString = "Pointer to static string allocated in some static memory";
    char localCString[] = "Local string - that is copied from some static memory";

    printCString(localkPointerToStaticCString);
    printCString(localCString);
    printCString("Static string allocated in some static memory");
}
