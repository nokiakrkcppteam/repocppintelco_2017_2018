#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

int main() {
    std::string personsA[5];
    std::vector<std::string> personsV;

    std::cout << sizeof(personsA) << '\n';
    std::cout << sizeof(int[5]) << '\n';
    std::cout << sizeof(personsV) << '\n';
    std::cout << sizeof(std::vector<int>) << '\n';

    personsA[0] = "John";
    personsV.push_back("Mary");
    std::cout << sizeof(personsA) << '\n';
    std::cout << sizeof(personsV) << '\n';

    std::copy(std::begin(personsA), std::end(personsA),
              std::ostream_iterator<std::string>(std::cout, "<<\n"));
    std::copy(personsV.begin(), personsV.end(),
              std::ostream_iterator<std::string>(std::cout, "<<\n"));


    personsA[1] = "Mark";
    personsV.push_back("Jane");
    std::cout << sizeof(personsA) << '\n';
    std::cout << sizeof(personsV) << '\n';

    std::copy(std::begin(personsA), std::end(personsA),
              std::ostream_iterator<std::string>(std::cout, "<<\n"));
    std::copy(personsV.begin(), personsV.end(),
              std::ostream_iterator<std::string>(std::cout, "<<\n"));


}
