#include <iostream>
#include "AA.hpp"

void printAllOnCopy(AA aa)
{
    for (int a : aa)
        std::cout << a << '-';
    std::cout << '\n';

    aa.push_back(3);
}

int main() 
{
    AA aa;
    aa.push_back(2);

    std::cout << ">> Function arguments passed by copy: \n";
    // In the following two calls
    // it is expected to print the same "2-"
    // because "aa.push_back(3)" has no observable effect to aa
    // on level of main:
    printAllOnCopy(aa);
    printAllOnCopy(aa);
}
