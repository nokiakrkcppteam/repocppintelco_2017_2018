#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
#include <cstdint>

using namespace std;
class Person {
public:
    using Name = string;
    using Age = uint16_t;
    Person(Name name, Age age)
        : name(std::move(name)), age(age)
    {}
    friend ostream& operator << (ostream& os,
                                 const Person& person)
    {
        return os << person.name << " " << person.age;
    }

    bool operator < (const Person& rhs) const
    {
        return name < rhs.name or
                name == rhs.name and age < rhs.age;
    }

    struct LessByName {
        bool operator ()(const Person& lhs,
                         const Person& rhs) {
            return lhs.name < rhs.name;
        }
    };

private:
    Name name;
    Age age;
};

template <typename C>
void printContainer(const C& c) {
    using output = ostream_iterator<typename C::value_type>;
    copy(begin(c), end(c), output(cout, "*"));
    cout << "\n";
}
int main() {
    set<string> names;
    set<Person> persons;
    set<Person, Person::LessByName> personsByName;

    auto insertToAll = [&](Person::Name name,
                           Person::Age age) {
        std::cout << "insert: name: " << name
                  << ", age: " << age << '\n';

        auto result1 = names.insert(name);
        auto result2 = persons.insert(Person{name, age});
        auto result3 = personsByName.insert(Person{name, age});

        std::cout << "success: " << result1.second
                                 << result2.second
                                 << result3.second
                                 << '\n';
        printContainer(names);
        printContainer(persons);
        printContainer(personsByName);
    };

    insertToAll("John", 23);
    insertToAll("Mary", 31);
    insertToAll("John", 48);
}
