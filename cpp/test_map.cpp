#include <iostream>
#include <map>
#include <algorithm>
#include <iterator>
#include <cstdint>

using namespace std;
template <typename M>
void printMap(const M& m) {
    for (auto&& keyValue : m) {
        cout << keyValue.first << ": "
             << keyValue.second << '\n';
    }
    cout << "-----------\n";
}
int main() {
    std::map<string, unsigned> personAges;

    personAges["John"] = 23;
    personAges["Mary"] = 31;
    printMap(personAges);
    personAges["John"] = 21;
    personAges["Mary"]++;
    printMap(personAges);
    if (personAges["Kate"] == 0) {
        printMap(personAges);
    }
    personAges.emplace("John", 48);
    printMap(personAges);
    auto result = personAges.emplace("John", 48);
    if (! result.second) {
        result.first->second = 48;
    }
    printMap(personAges);
}
