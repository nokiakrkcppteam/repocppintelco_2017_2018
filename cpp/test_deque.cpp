#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
#include <iterator>
#include <new>
#include <cstdlib>

std::size_t dynMemSize;
void* operator new (std::size_t size) {
    dynMemSize += size;
    std::size_t* memPlusSize = static_cast<std::size_t*>(
                std::malloc(size + sizeof(std::size_t)));
    *memPlusSize = size;
    return memPlusSize + 1;
}
void operator delete (void* mem) {
    std::size_t* memPlusSize = static_cast<std::size_t*>(mem) - 1;
    dynMemSize -= *memPlusSize;
    std::free(memPlusSize);
}

#define PRINT_IT(it) std::cout << #it ": " << it << '\n'
#define DYN_MEM_TEST(op) op; cout << #op " -> "; \
                         PRINT_IT(dynMemSize)

int main() {
    using namespace std;
    DYN_MEM_TEST((void*)0);
    DYN_MEM_TEST(deque<int> countersD);
    DYN_MEM_TEST(vector<int> countersV);

    PRINT_IT(sizeof(countersD));
    PRINT_IT(sizeof(countersV));

    DYN_MEM_TEST(countersD.push_back(1));
    DYN_MEM_TEST(countersV.push_back(2));
    DYN_MEM_TEST(countersD.push_front(3));
    DYN_MEM_TEST(countersV.insert(countersV.begin(), 4));

    copy(countersD.begin(), countersD.end(),
         ostream_iterator<int>(cout, "-"));
    copy(countersV.begin(), countersV.end(),
         ostream_iterator<int>(cout, "-"));
}
