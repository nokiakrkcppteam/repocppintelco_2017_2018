#include <iostream>
#include "AA_mutate.hpp"

void printAllOnPtr(const AA_mutate* aa) // Note - this is pointerto const object
// Const pointer to non-const object is: AA* const aa
{
    for (int a : *aa) // Note that star "*" needed to access value
        std::cout << a << '-';
    std::cout << '\n';

    aa->push_back(3); // we can do push_back because it is const function
    // not the type is AA_mutate - not AA
}

int main() 
{
    AA_mutate aa;
    aa.push_back(2);

    std::cout << ">> Function arguments passed by pointer: \n";
    // In the following two calls
    // it is expected second print will differ - since printAllOnPtr
    // because "aa->push_back(3)" has observable effect to aa
    // on level of main:
    printAllOnPtr(&aa); // note to make pointer to variable
    printAllOnPtr(&aa); // we need to use &
}
