#include <iostream>

// it would also woth AA_mutate
#include "AA_const_cast.hpp"

void printAllOnConstRef(const AA_const_cast& aa) // Note that reference is to const object
{
    for (int a : aa)
        std::cout << a << '-';
    std::cout << '\n';

    aa.push_back(3); // because for AA_const_cast type push_back is const - thet's why it can be called
}

int main() 
{
    AA_const_cast aa;
    aa.push_back(2);

    std::cout << ">> Function arguments passed by const reference: \n";
    // In the following two calls
    // it is expected second print will differ
    // because "aa.push_back(3)" has observable effect to aa
    // on level of main:
    printAllOnConstRef(aa);
    printAllOnConstRef(aa);
}
