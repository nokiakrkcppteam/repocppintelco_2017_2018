#include <iostream>
#include "Verbose.hpp"
#include <memory>

class A;
class B;

struct A
{
    Verbose  verbose{"A"};
    std::shared_ptr<B> b;
};
struct B
{
    Verbose  verbose{"B"};
    std::weak_ptr<A> a;
};

int main()
{
    Verbose guard("main");

    std::shared_ptr<A> a(new A());
    std::shared_ptr<B> b = std::make_shared<B>();
    std::cout << a.use_count() << std::endl;
    std::cout << b.use_count() << std::endl;

    a->b = b;
    b->a = a;

    std::cout << a.use_count() << std::endl;
    std::cout << b.use_count() << std::endl;
    std::cout << a->b.use_count() << std::endl;
    std::cout << b->a.use_count() << std::endl;

    std::shared_ptr<A> a2 = b->a.lock();
    if (a2)
    {
        std::cout << "A exists\n";
    }
    std::cout << a2.use_count();
    a2.reset();
}

