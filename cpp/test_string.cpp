#include <iostream>
#include <cstring>
#include <string>

int main() {
    char cstr1[20] = "ABCDE";
    char cstr2[10] = "GHA";
    std::string str1 = "ABCDE";
    std::string str2 = "GHA";

    std::cout << sizeof(cstr1) << ',' << std::strlen(cstr1) << ',' << cstr1 << '\n';
    std::cout << sizeof(cstr2) << ',' << std::strlen(cstr2) << ',' << cstr2 << '\n';
    std::cout << sizeof(str1) << ',' << str1.size() << ',' << str1.length() << ',' << str1 << '\n';
    std::cout << sizeof(str2) << ',' << str2.size() << ',' << str2.length() << ',' << str2 << '\n';
}
