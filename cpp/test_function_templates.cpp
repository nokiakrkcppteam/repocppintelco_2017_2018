#include <iostream>

int add(int a, int b) {
    return a + b;
}
float add(float a, float b) {
    return a + b;
}

template <typename T>
T addT(T a, T b) {
	return a + b;
}

int main() 
{
    std::cout << add(1, 7) << '\n';
    std::cout << add(1.1f, 7.3f) << '\n';
    std::cout << addT(1, 7) << '\n';
    std::cout << addT(1.1, 7.3) << '\n';
    std::cout << addT(1, 7) << '\n';
    std::cout << addT(1.1, 7.3) << '\n';

    // Exercise: rewrite addT - so can be called as addT(1, 7.3)
    std::cout << addT<float>(1, 7.3) << '\n';
}
