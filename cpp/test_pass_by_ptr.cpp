#include <iostream>
#include "AA.hpp"

void printAllOnPtr(AA* aa) // Note * between type and variable
{
    for (int a : *aa) // Note that star "*" needed to access value
        std::cout << a << '-';
    std::cout << '\n';

    aa->push_back(3); // note access via ->
}

int main() 
{
    AA aa;
    aa.push_back(2);

    std::cout << ">> Function arguments passed by pointer: \n";
    // In the following two calls
    // it is expected second print will differ - since printAllOnPtr
    // because "aa->push_back(3)" has observable effect to aa
    // on level of main:
    printAllOnPtr(&aa); // note to make pointer to variable
    printAllOnPtr(&aa); // we need to use &
}
