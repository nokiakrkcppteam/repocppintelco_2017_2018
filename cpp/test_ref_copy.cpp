#include <iostream>
#include "AA.hpp"

void printAll(AA& aa)
{
    for (int a : aa)
    {
        std::cout << a << '-';
    }
    std::cout << '\n';
    aa.push_back(3);
}

int main() 
{
    AA aa1;
    AA aa2;
    aa1.push_back(2);
    aa2.push_back(4);
    std::cout << "Two calls to printAll modifies aa1:\n";
    printAll(aa1); // aa1=="2,3}
    printAll(aa1); // aa1=={2.3.3}

    AA& bb1 = aa1;
    AA& bb2 = aa2;

    std::cout << "Two calls to printAll modifies aa1 - but via ref to it (bb1):\n";
    printAll(bb1); // aa1=="2,3,3,3}
    printAll(bb1); // aa1=={2.3.3,3,3}

    std::cout << "Copying references - means copying referred object!\n";
    printAll(aa2); // aa2=={4,3}
    printAll(bb2); // aa2=={4,3,3}
    bb2 = bb1;     // aa2==aa1
    printAll(aa2); // aa2=={2.3.3,3,3,3}
    printAll(aa1); // aa1=={2,3,3,3,3,3}
}
