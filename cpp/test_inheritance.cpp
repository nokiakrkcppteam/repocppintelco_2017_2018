#include <iostream>

struct A_priv
{
    int a = 1;
};

struct B_priv : private A_priv
{
    void f()
    {
        std::cout << a <<std::endl; // ok
    }
};

struct C_priv: public B_priv
{
    void f()
    {
        //std::cout << a << std::endl; //error: can't access a.
    }
};

void private_inheritance()
{
    B_priv b;
    b.f();
    //std::cout << b.a << std::endl; //error: can't access a.
    //A_priv& a = b; // error: can't cast to base class
}

struct A_protected
{
    int a = 2;
};

struct B_protected : protected A_protected
{
    void f()
    {
        std::cout << a + 1 <<std::endl; // ok
    }
};

struct C_protected: public B_protected
{
    void f(int)
    {
        std::cout << a + 3 << std::endl; //ok
    }
};

struct D_protected: public B_protected
{
    using B_protected::f;
    void f(int)
    {
        std::cout << a + 7 << std::endl; //ok
    }
};

void protected_inheritance()
{
    C_protected c;
    //c.f(); //error: B::f() is not visible
    c.f(0); // ok
    //std::cout << c.a << std::endl; //error: can't access a.
    D_protected d;
    d.f(); //ok
    d.f(0); // ok
    //std::cout << d.a << std::endl; //error: can't access a.
    B_protected& b = c; //ok
    //A_protected& a = c; //error: can't cast to base class
}

struct A_public
{
    int a = 11;
    int b = 0;
};

struct B_public: A_public
{
    int a = 22;
};

void public_inheritance()
{
    B_public b;
    std::cout << sizeof (B_public) << std::endl;
    std::cout << 2 * sizeof(int) << std::endl;
    std::cout << 3 * sizeof(int) << std::endl;

    std::cout << b.a << std::endl;
    std::cout << b.A_public::a << std::endl;
    A_public& a = b;
    std::cout << a.a << std::endl;
    std::cout << b.b << std::endl;

}

int main()
{
    private_inheritance();
    protected_inheritance();
    public_inheritance();

    return 0;
}
