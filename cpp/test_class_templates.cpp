#include <iostream>

struct SizedArrayOfInts {
    std::size_t size = 0;
    int array[20];
};
inline std::ostream& operator << (std::ostream& os, const SizedArrayOfInts& ints) {
    for (std::size_t i = 0u; i < ints.size; ++i)
        os << ints.array[i] << ' ';
    return os;
}

struct SizedArrayOfFloats {
    std::size_t size = 0;
    float array[30];
};

inline std::ostream& operator << (std::ostream& os, const SizedArrayOfFloats& floats) {
    for (std::size_t i = 0u; i < floats.size; ++i)
        os << floats.array[i] << ' ';
    return os;
}

template <typename ValueType, std::size_t ArraySize>
struct SizedArray {
    std::size_t size = 0;
    ValueType array[ArraySize];
};
template <typename ValueType, std::size_t ArraySize>
inline std::ostream& operator << (std::ostream& os, const SizedArray<ValueType, ArraySize>& values) {
    for (std::size_t i = 0u; i < values.size; ++i)
        os << values.array[i] << ' ';
    return os;
}

int main() {
    SizedArrayOfInts ints;
    SizedArrayOfFloats floats;

    ints.array[ints.size++] = 42;
    ints.array[ints.size++] = 24;
    floats.array[floats.size++] = 3.14;

    std::cout << ints << '\n';
    std::cout << floats << '\n';


    using SizedArrayOfIntsT = SizedArray<int, 20>;
    using SizedArrayOfFloatsT = SizedArray<float, 30>;
    SizedArrayOfIntsT intsT;
    SizedArrayOfFloatsT floatsT;

    intsT.array[intsT.size++] = 42;
    intsT.array[intsT.size++] = 24;
    floatsT.array[floatsT.size++] = 3.14;

    std::cout << intsT << '\n';
    std::cout << floatsT << '\n';

}
