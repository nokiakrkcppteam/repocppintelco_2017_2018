#pragma once

#include <gmock/gmock.h>
#include "UeConnection/IUeConnectionFactory.hpp"

namespace bts
{

class IUeConnectionFactoryMock : public IUeConnectionFactory
{
public:
    IUeConnectionFactoryMock();
    ~IUeConnectionFactoryMock() override;

    // Temporary solution until gtest/gmock supports unique_ptr as return value...
    MOCK_METHOD1(createConnectionMock, IUeConnection*(ITransportPtr));
    IUeRelay::UePtr createConnection(ITransportPtr transport) override;
};


}
