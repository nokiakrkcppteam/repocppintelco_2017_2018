#pragma once

#include <gmock/gmock.h>
#include "IConsole.hpp"

namespace bts
{

class IConsoleMock : public IConsole
{
public:
    IConsoleMock();
    ~IConsoleMock() override;

    MOCK_METHOD3(addCommand, void(std::string command, const std::string &commandText, CommandCallback));
    MOCK_METHOD3(addCloseCommand, void(std::string command, const std::string &commandText, CommandCallback));
    MOCK_METHOD2(addHelpCommand, void(std::string command, const std::string &commandText));
};


}
