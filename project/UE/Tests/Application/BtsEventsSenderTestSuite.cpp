#include "BtsEventsSenderTestSuite.hpp"
#include "Messages/IncomingMessage.hpp"

using namespace ::testing;

namespace ue
{

void BtsEventsSenderTestSuite::assertAttachRequest()
{
    common::IncomingMessage messageReader(message);
    common::MessageHeader header = messageReader.readMessageHeader();
    ASSERT_EQ(common::MessageId::AttachRequest, header.messageId);
    ASSERT_EQ(PHONE_NUMBER, header.from);
    ASSERT_EQ(common::PhoneNumber{}, header.to);
    common::BtsId btsId = messageReader.readBtsId();
    ASSERT_EQ(BTS_ID, btsId);
}

void BtsEventsSenderTestSuite::assertSms(ISmsDb::Sms const& sms)
{
    common::IncomingMessage messageReader(message);
    common::MessageHeader header = messageReader.readMessageHeader();
    ASSERT_EQ(common::MessageId::Sms, header.messageId);
    ASSERT_EQ(PHONE_NUMBER, header.from);
    ASSERT_EQ(OTHER_PHONE_NUMBER, header.to);
    std::uint8_t encryptionType = messageReader.readNumber<std::uint8_t>();
    const std::uint8_t NO_ENCRYPTION = 0;
    ASSERT_EQ(NO_ENCRYPTION, encryptionType);
    std::string smsText = messageReader.readRemainingText();
    ASSERT_EQ(sms.text, smsText);
}

void BtsEventsSenderTestSuite::expectMessageToBts()
{
    EXPECT_CALL(transportToBtsMock, sendMessage(_)).WillOnce(SaveArg<0>(&message));
}

TEST_F(BtsEventsSenderTestSuite, shallSendAttachRequest)
{
    expectMessageToBts();
    objectUnderTest.sendAttachRequest(BTS_ID);

   ASSERT_NO_THROW(assertAttachRequest());
}

TEST_F(BtsEventsSenderTestSuite, shallSendSms)
{
    expectMessageToBts();
    ISmsDb::Sms sms{PHONE_NUMBER, OTHER_PHONE_NUMBER, std::chrono::system_clock::time_point::min(), "Ala ma kota"};
    objectUnderTest.sendSms(sms);

   ASSERT_NO_THROW(assertSms(sms));
}

}
