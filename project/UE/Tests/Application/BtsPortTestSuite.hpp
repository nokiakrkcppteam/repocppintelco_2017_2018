#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "BtsPort.hpp"

#include "ApplicationBaseTestSuite.hpp"
#include "Mocks/IBtsEventsReceiverMock.hpp"
#include "Mocks/ITransportMock.hpp"

namespace ue
{

class BtsPortTestSuite : public ApplicationBaseTestSuite
{
protected:
    ::testing::ExpectationSet expectObjectUnderTestConstructed();

    common::ITransport::MessageCallback messageCallback;
    common::ITransport::DisconnectedCallback disconnectedCallback;

    ::testing::StrictMock<IBtsEventsReceiverMock> btsEventsReceiverMock{};
    ::testing::StrictMock<common::ITransportMock> transportToBtsMock{};

    ::testing::ExpectationSet constructorExpectations = expectObjectUnderTestConstructed();

    BtsPort objectUnderTest{btsEventsReceiverMock, PHONE_NUMBER, transportToBtsMock, loggerMock};
};

}
