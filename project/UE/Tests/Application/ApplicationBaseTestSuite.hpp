#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Mocks/ILoggerMock.hpp"
#include "Messages/BtsId.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class ApplicationBaseTestSuite : public ::testing::Test
{
protected:
    ApplicationBaseTestSuite();

    void expectLogErrors();
    void expectNoLogErrors();

    ::testing::NiceMock<common::ILoggerMock> loggerMock{};
    const common::PhoneNumber PHONE_NUMBER{231};
    const common::BtsId BTS_ID{123456878ul};
    const common::PhoneNumber OTHER_PHONE_NUMBER{123};
};

}
