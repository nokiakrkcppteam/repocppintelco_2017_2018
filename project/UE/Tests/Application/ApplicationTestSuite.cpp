#include "ApplicationTestSuite.hpp"

using namespace ::testing;

namespace ue
{

TEST_F(ApplicationNotConnectedTestSuite, shallSendAttachRequestOnSib)
{
    EXPECT_CALL(btsEventsSenderMock, sendAttachRequest(BTS_ID));
    EXPECT_CALL(timersMock, startAttachTimer());
    EXPECT_CALL(userMock, setConnecting());
    expectNoLogErrors();
    objectUnderTest.handleSib(BTS_ID);
}

ApplicationConnectingTestSuite::ApplicationConnectingTestSuite()
{
    EXPECT_CALL(btsEventsSenderMock, sendAttachRequest(BTS_ID));
    EXPECT_CALL(timersMock, startAttachTimer());
    EXPECT_CALL(userMock, setConnecting());
    expectNoLogErrors();
    objectUnderTest.handleSib(BTS_ID);
    Mock::VerifyAndClearExpectations(&btsEventsSenderMock);
    Mock::VerifyAndClearExpectations(&userMock);
    Mock::VerifyAndClearExpectations(&timersMock);
}

TEST_F(ApplicationConnectingTestSuite, shallRetryAttachAfterRejection)
{
    expectLogErrors();
    EXPECT_CALL(timersMock, stopAttachTimer());
    EXPECT_CALL(userMock, setNotConnected());
    objectUnderTest.handleAttachRejected();

    EXPECT_CALL(btsEventsSenderMock, sendAttachRequest(BTS_ID));
    EXPECT_CALL(userMock, setConnecting());
    EXPECT_CALL(timersMock, startAttachTimer());
    objectUnderTest.handleSib(BTS_ID);

    EXPECT_CALL(timersMock, stopAttachTimer());
    EXPECT_CALL(userMock, setMainMenu());
    objectUnderTest.handleAttachAccepted();
}

TEST_F(ApplicationConnectingTestSuite, shallRetryAttachAfterTimeout)
{
    expectLogErrors();
    EXPECT_CALL(userMock, setNotConnected());
    objectUnderTest.handleAttachTimeout();

    EXPECT_CALL(btsEventsSenderMock, sendAttachRequest(BTS_ID));
    EXPECT_CALL(userMock, setConnecting());
    EXPECT_CALL(timersMock, startAttachTimer());
    objectUnderTest.handleSib(BTS_ID);

    EXPECT_CALL(timersMock, stopAttachTimer());
    EXPECT_CALL(userMock, setMainMenu());
    objectUnderTest.handleAttachAccepted();
}

TEST_F(ApplicationConnectingTestSuite, shallAcceptAttachAccepted)
{
    EXPECT_CALL(userMock, setMainMenu());
    EXPECT_CALL(timersMock, stopAttachTimer());
    expectNoLogErrors();
    objectUnderTest.handleAttachAccepted();
}

TEST_F(ApplicationConnectingTestSuite, shallComplainOnAttachRejected)
{
    EXPECT_CALL(userMock, setNotConnected());
    EXPECT_CALL(timersMock, stopAttachTimer());
    expectLogErrors();
    objectUnderTest.handleAttachRejected();
}

TEST_F(ApplicationConnectingTestSuite, shallComplainOnAttachTimeout)
{
    EXPECT_CALL(userMock, setNotConnected());
    expectLogErrors();
    objectUnderTest.handleAttachTimeout();
}

TEST_F(ApplicationConnectingTestSuite, shallRetryConnectingOnSibWithComplaining)
{
    EXPECT_CALL(btsEventsSenderMock, sendAttachRequest(BTS_ID));
    EXPECT_CALL(timersMock, startAttachTimer());
    expectLogErrors();

    objectUnderTest.handleSib(BTS_ID);
}

ApplicationConnectedTestSuite::ApplicationConnectedTestSuite()
{
    EXPECT_CALL(userMock, setMainMenu());
    EXPECT_CALL(timersMock, stopAttachTimer());
    expectNoLogErrors();
    objectUnderTest.handleAttachAccepted();
    Mock::VerifyAndClearExpectations(&userMock);
    Mock::VerifyAndClearExpectations(&timersMock);
}

TEST_F(ApplicationConnectedTestSuite, shallComplainOnSib)
{
    expectLogErrors();
    objectUnderTest.handleSib(BTS_ID);
}

TEST_F(ApplicationConnectedTestSuite, shallReceiveSms)
{
    ISmsDb::Sms sms{OTHER_PHONE_NUMBER, PHONE_NUMBER, std::chrono::system_clock::time_point::min(), "Ala ma kota"};
    auto matchSms = AllOf(Field(&ISmsDb::Sms::from, OTHER_PHONE_NUMBER),
                          Field(&ISmsDb::Sms::to, PHONE_NUMBER),
                          Field(&ISmsDb::Sms::text, sms.text));
    expectNoLogErrors();
    EXPECT_CALL(smsDbMock, addSms(matchSms));
    objectUnderTest.handleSmsReceived(sms);
}

}
