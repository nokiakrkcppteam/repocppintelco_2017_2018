#include "ApplicationBaseTestSuite.hpp"

using namespace ::testing;

namespace ue
{

ApplicationBaseTestSuite::ApplicationBaseTestSuite()
{
    EXPECT_CALL(loggerMock, log(Ne(common::ILogger::ERROR_LEVEL), _)).Times(AnyNumber());
}

void ApplicationBaseTestSuite::expectLogErrors()
{
    EXPECT_CALL(loggerMock, log(common::ILogger::ERROR_LEVEL, _)).Times(AtLeast(1));
}

void ApplicationBaseTestSuite::expectNoLogErrors()
{
    EXPECT_CALL(loggerMock, log(common::ILogger::ERROR_LEVEL, _)).Times(0);
}

}
