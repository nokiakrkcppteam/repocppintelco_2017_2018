#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "BtsEventsSender.hpp"

#include "ApplicationBaseTestSuite.hpp"
#include "Mocks/IBtsEventsReceiverMock.hpp"
#include "Mocks/ITransportMock.hpp"

namespace ue
{

class BtsEventsSenderTestSuite : public ApplicationBaseTestSuite
{
protected:
    void expectMessageToBts();
    void assertAttachRequest();
    void assertSms(const ISmsDb::Sms &sms);

    common::BinaryMessage message{};
    ::testing::StrictMock<common::ITransportMock> transportToBtsMock{};
    BtsEventsSender objectUnderTest{PHONE_NUMBER, transportToBtsMock, loggerMock};
};

}
