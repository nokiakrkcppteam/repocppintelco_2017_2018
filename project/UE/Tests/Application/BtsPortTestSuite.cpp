#include "BtsPortTestSuite.hpp"
#include "Messages/OutgoingMessage.hpp"

using namespace ::testing;

namespace ue
{

ExpectationSet BtsPortTestSuite::expectObjectUnderTestConstructed()
{
    ExpectationSet result;
    result += EXPECT_CALL(transportToBtsMock, registerMessageCallback(_)).WillOnce(SaveArg<0>(&messageCallback));
    result += EXPECT_CALL(transportToBtsMock, registerDisconnectedCallback(_)).WillOnce(SaveArg<0>(&disconnectedCallback));
    return result;
}


TEST_F(BtsPortTestSuite, shallRegisterBtsCallbacks)
{
    ASSERT_TRUE(static_cast<bool>(messageCallback));
    ASSERT_TRUE(static_cast<bool>(disconnectedCallback));
}

TEST_F(BtsPortTestSuite, shallForwardSibToReceiver)
{
    EXPECT_CALL(btsEventsReceiverMock, handleSib(BTS_ID));

    common::OutgoingMessage sibBuilder{common::MessageId::Sib, common::PhoneNumber{}, PHONE_NUMBER};
    sibBuilder.writeBtsId(BTS_ID);

    ASSERT_NO_THROW(messageCallback(sibBuilder.getMessage()));
}

TEST_F(BtsPortTestSuite, shallForwardAttachAcceptedToReceiver)
{
    EXPECT_CALL(btsEventsReceiverMock, handleAttachAccepted());

    common::OutgoingMessage attachResponseBuilder{common::MessageId::AttachResponse, common::PhoneNumber{}, PHONE_NUMBER};
    attachResponseBuilder.writeNumber(true);

    ASSERT_NO_THROW(messageCallback(attachResponseBuilder.getMessage()));
}

TEST_F(BtsPortTestSuite, shallForwardAttachRejectedToReceiver)
{
    EXPECT_CALL(btsEventsReceiverMock, handleAttachRejected());

    common::OutgoingMessage attachResponseBuilder{common::MessageId::AttachResponse, common::PhoneNumber{}, PHONE_NUMBER};
    attachResponseBuilder.writeNumber(false);

    ASSERT_NO_THROW(messageCallback(attachResponseBuilder.getMessage()));
}

TEST_F(BtsPortTestSuite, shallForwardSmsToReceiver)
{
    const std::string smsText = "Ala ma kota";
    auto matchSms = AllOf(Field(&ISmsDb::Sms::from, OTHER_PHONE_NUMBER),
                          Field(&ISmsDb::Sms::to, PHONE_NUMBER),
                          Field(&ISmsDb::Sms::text, smsText));

    EXPECT_CALL(btsEventsReceiverMock, handleSmsReceived(matchSms));

    common::OutgoingMessage smsBuilder{common::MessageId::Sms, OTHER_PHONE_NUMBER, PHONE_NUMBER};
    smsBuilder.writeNumber<std::uint8_t>(0); // no encryption
    smsBuilder.writeText(smsText);

    ASSERT_NO_THROW(messageCallback(smsBuilder.getMessage()));
}

TEST_F(BtsPortTestSuite, shallComplainOnUexpectedEvent)
{
    common::OutgoingMessage unexpectedBuilder{common::MessageId::AttachRequest, common::PhoneNumber{}, PHONE_NUMBER};

    expectLogErrors();

    ASSERT_NO_THROW(messageCallback(unexpectedBuilder.getMessage()));
}

}
