#include "TimersTestSuite.hpp"

using namespace ::testing;

namespace ue
{

void TimersTestSuite::sleep(TimersTestSuite::Duration duration)
{
    std::this_thread::sleep_for(duration);
}

TEST_F(TimersTestSuite, shallStartAttachTimerWhenMechanismNotStarted)
{
    expectNoLogErrors();
    EXPECT_CALL(timeProviderMock, timeFromNow(_)).WillOnce(Return(TIME1));
    objectUnderTest.startAttachTimer();
}

TEST_F(TimersTestSuite, shallStartStopAttachTimerWhenMechanismNotStarted)
{
    expectNoLogErrors();
    EXPECT_CALL(timeProviderMock, timeFromNow(_)).WillOnce(Return(TIME1));
    objectUnderTest.startAttachTimer();
    objectUnderTest.stopAttachTimer();
}

TEST_F(TimersTestSuite, shallComplainOnStopWithoutStartForAttachTimerEvenWhenMechanismNotStarted)
{
    expectLogErrors();
    objectUnderTest.stopAttachTimer();
}

TEST_F(TimersTestSuite, shallStartStop)
{
    expectNoLogErrors();
    EXPECT_CALL(timeProviderMock, wait())
            .WillOnce(Return(TIME1))
            .WillOnce(DoAll(Invoke([&] { sleep(TIME); }), Return(TIME2)));
    objectUnderTest.start(timeoutsReceiverMock);
    sleep(HALF_TIME);
    objectUnderTest.stop();
}

TEST_F(TimersTestSuite, shallStartStopAttachTimer)
{
    expectNoLogErrors();

    EXPECT_CALL(timeProviderMock, wait())
            .WillOnce(Return(TIME1))
            .WillOnce(DoAll(Invoke([&] { sleep(TIME); }), Return(TIME2)));
    objectUnderTest.start(timeoutsReceiverMock);

    EXPECT_CALL(timeProviderMock, timeFromNow(_)).WillOnce(Return(TIME4));
    objectUnderTest.startAttachTimer();

    sleep(HALF_TIME);
    objectUnderTest.stopAttachTimer();
    objectUnderTest.stop();
}

TEST_F(TimersTestSuite, shallStartExpireAttachTimer)
{
    expectNoLogErrors();
    EXPECT_CALL(timeoutsReceiverMock, handleAttachTimeout());

    EXPECT_CALL(timeProviderMock, wait())
            .WillOnce(Return(TIME1))
            .WillOnce(DoAll(Invoke([&] { sleep(HALF_TIME); }), Return(TIME3)))
            .WillOnce(DoAll(Invoke([&] { sleep(TIME); }), Return(TIME4)));
    objectUnderTest.start(timeoutsReceiverMock);

    EXPECT_CALL(timeProviderMock, timeFromNow(_)).WillOnce(Return(TIME2));
    objectUnderTest.startAttachTimer();
    // sleep - see wait above

    sleep(TIME);
    objectUnderTest.stop();
}

TEST_F(TimersTestSuite, shallComplainOnStopAfterExpireAttachTimer)
{
    expectLogErrors();
    EXPECT_CALL(timeoutsReceiverMock, handleAttachTimeout());

    EXPECT_CALL(timeProviderMock, wait())
            .WillOnce(Return(TIME1))
            .WillOnce(DoAll(Invoke([&] { sleep(HALF_TIME); }), Return(TIME3)))
            .WillOnce(DoAll(Invoke([&] { sleep(TIME); }), Return(TIME4)));
    objectUnderTest.start(timeoutsReceiverMock);

    EXPECT_CALL(timeProviderMock, timeFromNow(_)).WillOnce(Return(TIME2));
    objectUnderTest.startAttachTimer();
    // sleep - see wait above

    sleep(TIME);
    objectUnderTest.stopAttachTimer();
    objectUnderTest.stop();
}

}
