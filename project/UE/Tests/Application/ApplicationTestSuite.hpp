#pragma once

#include <gmock/gmock.h>

#include "Application.hpp"

#include "ApplicationBaseTestSuite.hpp"
#include "Mocks/IBtsEventsSenderMock.hpp"
#include "Mocks/IUserMock.hpp"
#include "Mocks/ITimersMock.hpp"
#include "Mocks/ISmsDbMock.hpp"

namespace ue
{

class ApplicationTestSuite : public ApplicationBaseTestSuite
{
protected:
    ::testing::StrictMock<IBtsEventsSenderMock> btsEventsSenderMock{};
    ::testing::StrictMock<IUserMock> userMock{};
    ::testing::StrictMock<ITimersMock> timersMock{};
    ::testing::StrictMock<ISmsDbMock> smsDbMock{};
    Application objectUnderTest{PHONE_NUMBER, btsEventsSenderMock, userMock, timersMock, smsDbMock, loggerMock};
};

using ApplicationNotConnectedTestSuite = ApplicationTestSuite;
class ApplicationConnectingTestSuite : public ApplicationTestSuite
{
public:
    ApplicationConnectingTestSuite();
};
class ApplicationConnectedTestSuite : public ApplicationConnectingTestSuite
{
public:
    ApplicationConnectedTestSuite();
};

}
