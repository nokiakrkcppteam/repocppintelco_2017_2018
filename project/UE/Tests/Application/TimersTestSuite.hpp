#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Timers.hpp"
#include "ApplicationBaseTestSuite.hpp"
#include "Mocks/ITimeoutsReceiverMock.hpp"
#include "Mocks/ITimersMock.hpp"

namespace ue
{

using namespace std::literals::chrono_literals;
class TimersTestSuite : public ApplicationBaseTestSuite
{
protected:
    using TimePoint = Timers::TimePoint;
    using Duration = Timers::Duration;
    const TimePoint ZERO_TIME = TimePoint::min();
    const TimePoint TIME1 = ZERO_TIME + 100s;
    const TimePoint TIME2 = ZERO_TIME + 200s;
    const TimePoint TIME3 = ZERO_TIME + 300s;
    const TimePoint TIME4 = ZERO_TIME + 400s;
    const Duration TIME = 100ms;
    const Duration HALF_TIME = TIME / 2;

    ::testing::StrictMock<ITimeoutsReceiverMock> timeoutsReceiverMock{};
    ::testing::StrictMock<ITimeProviderMock> timeProviderMock{};

    ::testing::Expectation expectZeroAlways = EXPECT_CALL(timeProviderMock, zeroTime()).WillRepeatedly(::testing::Return(ZERO_TIME));

    Timers objectUnderTest{loggerMock, timeProviderMock};

    void sleep(Duration duration);
};

}
