#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ITimeoutsReceiver.hpp"

namespace ue
{

class ITimeoutsReceiverMock : public ITimeoutsReceiver
{
public:
    ITimeoutsReceiverMock();
    ~ITimeoutsReceiverMock() override;

    MOCK_METHOD0(handleAttachTimeout, void());
};

}
