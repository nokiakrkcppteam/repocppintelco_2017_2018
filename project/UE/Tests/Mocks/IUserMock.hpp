#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "IUser.hpp"

namespace ue
{

class IUserMock : public IUser
{
public:
    IUserMock();
    ~IUserMock() override;
    MOCK_METHOD1(handleAccept, void(IUserEventsReceiver&));
    MOCK_METHOD0(setNotConnected, void());
    MOCK_METHOD0(setConnecting, void());
    MOCK_METHOD0(setMainMenu, void());
    MOCK_METHOD0(setComposeSms, void());
    MOCK_METHOD1(setSmsListView, void(const ISmsDb &));
    MOCK_METHOD1(setSmsView, void(const ISmsDb::Sms &));
};

}
