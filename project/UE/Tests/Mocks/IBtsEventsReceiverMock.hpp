#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "IBtsEventsReceiver.hpp"

#include "Messages/BtsId.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class IBtsEventsReceiverMock : public IBtsEventsReceiver
{
public:
    IBtsEventsReceiverMock();
    ~IBtsEventsReceiverMock() override;
    MOCK_METHOD1(handleSib, void(common::BtsId));
    MOCK_METHOD0(handleAttachAccepted, void());
    MOCK_METHOD0(handleAttachRejected, void());
    MOCK_METHOD1(handleSmsReceived, void(ISmsDb::Sms const&));
};

}
