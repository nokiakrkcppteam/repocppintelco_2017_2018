#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "IBtsEventsSender.hpp"

#include "Messages/BtsId.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class IBtsEventsSenderMock : public IBtsEventsSender
{
public:
    IBtsEventsSenderMock();
    ~IBtsEventsSenderMock() override;
    MOCK_METHOD1(sendAttachRequest, void(common::BtsId));
    MOCK_METHOD1(sendSms, void(const ISmsDb::Sms &sms));
};

}
