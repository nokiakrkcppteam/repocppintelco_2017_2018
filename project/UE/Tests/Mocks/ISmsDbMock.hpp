#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ISmsDb.hpp"

namespace ue
{

class ISmsDbMock : public ISmsDb
{
public:
    ISmsDbMock();
    ~ISmsDbMock() override;

    MOCK_METHOD1(addSms, void(Sms const&));
    MOCK_CONST_METHOD1(readSmses, void(Reader));
};

}
