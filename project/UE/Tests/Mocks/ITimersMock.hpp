#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ITimers.hpp"
#include "Timers.hpp"

namespace ue
{

class ITimersMock : public ITimers
{
public:
    ITimersMock();
    ~ITimersMock() override;
    MOCK_METHOD0(startAttachTimer, void());
    MOCK_METHOD0(stopAttachTimer, void());
};

class ITimeProviderMock : public Timers::ITimeProvider
{
public:
    ITimeProviderMock();
    ~ITimeProviderMock() override;
    MOCK_METHOD0(wait, Timers::TimePoint());
    MOCK_CONST_METHOD1(timeFromNow, Timers::TimePoint(Timers::Duration));
    MOCK_CONST_METHOD0(zeroTime, Timers::TimePoint());
};

}
