#pragma once

namespace ue
{

class ITimeoutsReceiver
{
public:
    virtual ~ITimeoutsReceiver() = default;

    virtual void handleAttachTimeout() = 0;
};

}
