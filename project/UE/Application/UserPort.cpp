#include "UserPort.hpp"

namespace ue
{

UserPort::UserPort(IUserEventsReceiver &eventsReceiver,
                   IUser &user,
                   IUeGui &ueGui,
                   common::ILogger &logger)
    : eventsReceiver(eventsReceiver),
      user(user),
      ueGui(ueGui),
      logger(logger)
{
    ueGui.setCloseGuard(std::bind(&UserPort::handleExitUe, this));
    ueGui.setAcceptCallback(std::bind(&UserPort::handleAccept, this));
    ueGui.setRejectCallback(std::bind(&UserPort::handleReject, this));
}

IUeGui::AcceptClose UserPort::handleExitUe()
{
    logger.logInfo("Exist UE");
    eventsReceiver.handleExitUe();
}

void UserPort::handleAccept()
{
    logger.logDebug("Accept in current view");
    user.handleAccept(eventsReceiver);
}

void UserPort::handleReject()
{
    logger.logDebug("Exit current view");
    eventsReceiver.handleExitCurrentView();
}


}
