#pragma once

#include "ISmsDb.hpp"
#include <vector>

namespace ue
{

class SmsDb : public ISmsDb
{
public:
    void addSms(const Sms& sms) override;
    void readSmses(Reader reader) const override;

private:
    std::vector<Sms> smses;
};

}
