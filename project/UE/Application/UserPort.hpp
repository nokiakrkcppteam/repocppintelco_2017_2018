#pragma once

#include "IUser.hpp"
#include "IUserEventsReceiver.hpp"
#include "Logger/ILogger.hpp"
#include "IUeGui.hpp"

namespace ue
{

class UserPort
{
public:
    UserPort(IUserEventsReceiver& eventsReceiver,
             IUser& user,
             IUeGui& ueGui,
             common::ILogger& logger);

private:
    IUeGui::AcceptClose handleExitUe();
    void handleAccept();
    void handleReject();


    IUserEventsReceiver& eventsReceiver;
    IUser& user;
    IUeGui& ueGui;
    common::ILogger& logger;
};

}
