#pragma once

#include "IBtsEventsSender.hpp"

#include "Messages.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Messages/BtsId.hpp"
#include "ITransport.hpp"
#include "Logger/ILogger.hpp"

namespace ue
{

class BtsEventsSender : public IBtsEventsSender
{
public:
    BtsEventsSender(common::PhoneNumber phoneNumber, common::ITransport&transportToBts, common::ILogger&logger);
    void sendAttachRequest(common::BtsId btsId) override;
    void sendSms(const ISmsDb::Sms &sms) override;

private:
    common::PhoneNumber phoneNumber;
    common::ITransport& transportToBts;
    common::ILogger& logger;
};

}
