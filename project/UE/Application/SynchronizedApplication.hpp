#pragma once

#include "IEventsReceiver.hpp"
#include <mutex>

namespace ue
{

class SynchronizedApplication : public IEventsReceiver
{
public:

    SynchronizedApplication(IEventsReceiver& eventsReceiver);

    // IBtsEventsReceiver
    void handleSib(common::BtsId btsId) override;
    void handleAttachAccepted() override;
    void handleAttachRejected() override;
    void handleSmsReceived(const ISmsDb::Sms &sms);

    // IUserEventsReceiver interface
    IUeGui::AcceptClose handleExitUe();
    void handleExitCurrentView();
    void handleViewSmses();
    void handleViewSms(const ISmsDb::Sms &sms);
    void handleComposeSms();
    void handleSmsComposed(const ISmsDb::Sms &sms);

    // ITimeoutsReceiver
    void handleAttachTimeout() override;

private:
    using Lock = std::lock_guard<std::mutex>;
    std::mutex synchGuard;

    IEventsReceiver& eventsReceiver;
};

}
