#pragma once

#include "IEventsReceiver.hpp"
#include "IBtsEventsSender.hpp"
#include "IUser.hpp"
#include "ITimers.hpp"
#include "Logger/ILogger.hpp"
#include "Messages/PhoneNumber.hpp"
#include <memory>
#include "States/StateFactory.hpp"

namespace ue
{

class Application : public IEventsReceiver
{
public:
    Application(common::PhoneNumber phoneNumber,
                IBtsEventsSender &btsEventsSender,
                IUser& user,
                ITimers &timers,
                ISmsDb& smsDb,
                common::ILogger& logger);

    void handleSib(common::BtsId) override;
    void handleAttachAccepted() override;
    void handleAttachRejected() override;
    void handleAttachTimeout() override;
    void handleSmsReceived(const ISmsDb::Sms &sms) override;

    // IUserEventsReceiver interface
    IUeGui::AcceptClose handleExitUe() override;
    void handleExitCurrentView() override;
    void handleViewSmses() override;
    void handleViewSms(const ISmsDb::Sms &sms) override;
    void handleComposeSms() override;
    void handleSmsComposed(const ISmsDb::Sms &sms) override;


    void setState(std::shared_ptr<State>);

private:
    common::PhoneNumber phoneNumber;
    IBtsEventsSender& btsEventsSender;
    IUser& user;
    ITimers &timers;
    ISmsDb& smsDb;
    common::ILogger& logger;
    StateFactory stateFactory{*this, btsEventsSender, user, timers, smsDb, logger};

    std::shared_ptr<State> state;

};

}
