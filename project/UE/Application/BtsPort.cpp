#include "BtsPort.hpp"

#include "Messages/IncomingMessage.hpp"
#include "Messages/BinaryMessage.hpp"
#include "ISmsDb.hpp"


namespace ue
{

BtsPort::BtsPort(IBtsEventsReceiver &eventsReceiver,
                 common::PhoneNumber phoneNumber,
                 common::ITransport &transportToBts,
                 common::ILogger &logger)
    : eventsReceiver(eventsReceiver),
      phoneNumber(phoneNumber),
      transportToBts(transportToBts),
      logger(logger)
{
    transportToBts.registerDisconnectedCallback([this]
    {
        handleDisconnect();
    });
    transportToBts.registerMessageCallback([this](BinaryMessage message)
    {
        handleMessage(message);
    });

}

void BtsPort::handleDisconnect()
{
    logger.logInfo("Disconnected from BTS");
}

void BtsPort::handleMessage(const common::BinaryMessage& message)
{
    common::IncomingMessage messageReader(message);
    common::MessageHeader header = messageReader.readMessageHeader();

    logger.logInfo("Message received: ", header.messageId);

    switch (header.messageId)
    {
    case common::MessageId::Sib:
        eventsReceiver.handleSib(messageReader.readBtsId());
        break;

    case common::MessageId::AttachResponse:
        {
            bool accepted = messageReader.readNumber<uint8_t>();
            if (accepted)
            {
                eventsReceiver.handleAttachAccepted();
            }
            else
            {
                eventsReceiver.handleAttachRejected();
            }
        }
        break;

    case common::MessageId::Sms:
        {
            readMessageEncryption(messageReader);
            ISmsDb::Sms sms{};
            sms.from = header.from;
            sms.to = header.to;
            sms.text = messageReader.readRemainingText();
            sms.when = std::chrono::system_clock::now();
            eventsReceiver.handleSmsReceived(sms);
        }
        break;

    default:
        logger.logError("Message handling not implemented: ", header.messageId);
        break;
    }
}

void BtsPort::readMessageEncryption(common::IncomingMessage& messageReader)
{
    auto encryptionType = messageReader.readNumber<uint8_t>();
    if (encryptionType != 0)
    {
        logger.logError("Not supported: ", encryptionType, " - data will be corrupted");
    }
}

}
