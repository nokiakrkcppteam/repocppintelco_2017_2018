#pragma once

#include "IBtsEventsReceiver.hpp"
#include "Logger/ILogger.hpp"
#include "ITransport.hpp"

#include "Messages.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Messages/BtsId.hpp"

namespace common
{
class IncomingMessage;
}

namespace ue
{

class BtsPort
{
public:
    BtsPort(IBtsEventsReceiver& eventsReceiver,
            common::PhoneNumber phoneNumber,
            common::ITransport& transportToBts,
            common::ILogger& logger);

private:
    void handleDisconnect();
    void handleMessage(const BinaryMessage&);
    void readMessageEncryption(common::IncomingMessage &messageReader);

    IBtsEventsReceiver& eventsReceiver;
    common::PhoneNumber phoneNumber;
    ITransport& transportToBts;
    common::ILogger& logger;
};

}
