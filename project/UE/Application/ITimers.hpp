#pragma once

namespace ue
{

class ITimers
{
public:
    virtual ~ITimers() = default;

    virtual void startAttachTimer() = 0;
    virtual void stopAttachTimer() = 0;
};

}

