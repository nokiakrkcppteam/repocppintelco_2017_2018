#pragma once

#include "Messages.hpp"
#include "Messages/BtsId.hpp"
#include "ISmsDb.hpp"

namespace ue
{

class IBtsEventsSender
{
public:
    virtual ~IBtsEventsSender() = default;
    virtual void sendAttachRequest(common::BtsId btsId) = 0;
    virtual void sendSms(const ISmsDb::Sms &sms) = 0;
};

}
