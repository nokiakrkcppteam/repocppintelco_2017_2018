#pragma once

#include "IUser.hpp"

#include "Messages/PhoneNumber.hpp"
#include "Logger/ILogger.hpp"
#include "IUeGui.hpp"

#include <functional>
#include <vector>
#include <string>

namespace ue
{

class User : public IUser
{
public:
    User(common::PhoneNumber phoneNumber, IUeGui& ueGui, common::ILogger&logger);

    void handleAccept(IUserEventsReceiver& eventsReceiver) override;

    void setNotConnected() override;
    void setConnecting() override;
    void setMainMenu() override;
    void setComposeSms() override;
    void setSmsListView(const ISmsDb& smsDb) override;
    void setSmsView(const ISmsDb::Sms& sms) override;

private:
    using AcceptHandler = std::function<void(IUserEventsReceiver&)>;
    using Smses = std::vector<ISmsDb::Sms>;
    struct MenuOption
    {
        std::string name;
        std::string tooltip;
        AcceptHandler acceptHandler;
    };
    using MenuOptions = std::vector<MenuOption>;

    void noActionForAccept(IUserEventsReceiver& eventsReceiver);

    void startSmsCompose(IUserEventsReceiver& eventsReceiver);
    void startSmsListView(IUserEventsReceiver& eventsReceiver);

    std::string shortSmsText(const ISmsDb::Sms& sms) const;
    std::string fullSmsText(const ISmsDb::Sms &sms) const;
    auto smsTime(const ISmsDb::Sms &sms) const;
    void smsComposed(IUserEventsReceiver& eventsReceiver, const ISmsDb::Sms& sms);


    common::PhoneNumber phoneNumber;
    IUeGui& ueGui;
    common::ILogger& logger;

    MenuOptions menuOptions;
    AcceptHandler currentAcceptHandler;
};

}
