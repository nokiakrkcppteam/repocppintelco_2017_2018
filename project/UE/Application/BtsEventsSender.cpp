#include "BtsEventsSender.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "Messages/BinaryMessage.hpp"


namespace ue
{

BtsEventsSender::BtsEventsSender(common::PhoneNumber phoneNumber,
                                 common::ITransport &transportToBts,
                                 common::ILogger &logger)
    : phoneNumber(phoneNumber),
      transportToBts(transportToBts),
      logger(logger)
{}

void BtsEventsSender::sendAttachRequest (common::BtsId btsId)
{
    logger.logInfo("Send attach request: ", btsId);
    common::OutgoingMessage messageWriter(common::MessageId::AttachRequest,
                                          phoneNumber, {} );
    messageWriter.writeBtsId(btsId);
    transportToBts.sendMessage(messageWriter.getMessage());
}

void BtsEventsSender::sendSms(const ISmsDb::Sms &sms)
{
    logger.logDebug("Send sms to: ", sms.to);
    common::OutgoingMessage messageWriter(common::MessageId::Sms,
                                          sms.from, sms.to );
    messageWriter.writeNumber<std::uint8_t>(0); // no encryption
    messageWriter.writeText(sms.text);
    transportToBts.sendMessage(messageWriter.getMessage());
}

}
