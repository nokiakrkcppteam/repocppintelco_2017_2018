#include "Application.hpp"
#include "States/State.hpp"

namespace ue
{

Application::Application(common::PhoneNumber phoneNumber,
                         IBtsEventsSender &btsEventsSender,
                         IUser &user,
                         ITimers &timers,
                         ISmsDb &smsDb,
                         common::ILogger& logger)
  : phoneNumber(phoneNumber),
    btsEventsSender(btsEventsSender),
    user(user),
    timers(timers),
    smsDb(smsDb),
    logger(logger),
    state(stateFactory.getNotConnectedState())
{}

void Application::handleSib(common::BtsId btsId)
{
    logger.logDebug("Handle Sib: ", btsId, " in state: ", state->getName());
    state->handleSib(btsId);
}

void Application::handleAttachAccepted()
{
    logger.logDebug("Handle AttachAccepted in state: ", state->getName());
     state->handleAttachAccepted();
}

void Application::handleAttachRejected()
{
    logger.logDebug("Handle AttachRejected in state: ", state->getName());
    state->handleAttachRejected();
}

void Application::handleAttachTimeout()
{
    logger.logDebug("Handle AttachTimeout in state: ", state->getName());
    state->handleAttachTimeout();
}

void Application::handleSmsReceived(const ISmsDb::Sms &sms)
{
    logger.logDebug("Handle SmsReceived in state: ", state->getName());
    state->handleSmsReceived(sms);
}

IUeGui::AcceptClose Application::handleExitUe()
{
    logger.logDebug("Handle ExitUe in state: ", state->getName());
    return state->handleExitUe();
}

void Application::handleExitCurrentView()
{
    logger.logDebug("Handle ExitCurrentView in state: ", state->getName());
    state->handleExitCurrentView();
}

void Application::handleViewSmses()
{
    logger.logDebug("Handle ViewSmses in state: ", state->getName());
    state->handleViewSmses();
}

void Application::handleViewSms(const ISmsDb::Sms &sms)
{
    logger.logDebug("Handle ViewSms in state: ", state->getName());
    state->handleViewSms(sms);
}

void Application::handleComposeSms()
{
    logger.logDebug("Handle ComposeSms in state: ", state->getName());
    state->handleComposeSms();
}

void Application::handleSmsComposed(const ISmsDb::Sms &sms)
{
    logger.logDebug("Handle SmsComposed in state: ", state->getName());
    state->handleSmsComposed(sms);
}

void Application::setState(std::shared_ptr<State> newState)
{
    state=newState;
}

}
