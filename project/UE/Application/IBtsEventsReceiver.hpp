#pragma once

#include "Messages.hpp"
#include "Messages/BtsId.hpp"
#include "ISmsDb.hpp"

namespace ue
{

class IBtsEventsReceiver
{
public:
    virtual ~IBtsEventsReceiver() = default;

    virtual void handleSib(common::BtsId) = 0;
    virtual void handleAttachAccepted() = 0;
    virtual void handleAttachRejected() = 0;
    virtual void handleSmsReceived(const ISmsDb::Sms& sms) = 0;
};

}
