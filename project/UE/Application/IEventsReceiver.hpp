#pragma once

#include "IBtsEventsReceiver.hpp"
#include "IUserEventsReceiver.hpp"
#include "ITimeoutsReceiver.hpp"

namespace ue
{

class IEventsReceiver : public IBtsEventsReceiver,
                        public IUserEventsReceiver,
                        public ITimeoutsReceiver
{};

}
