#pragma once

#include <string>
#include <chrono>
#include <functional>
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class ISmsDb
{
public:
    using TimePoint = std::chrono::system_clock::time_point;
    struct Sms
    {
        common::PhoneNumber from;
        common::PhoneNumber to;
        TimePoint when;
        std::string text;
    };
    using Reader = std::function<bool(const Sms&)>;

    virtual ~ISmsDb() = default;

    virtual void addSms(const Sms&) = 0;
    virtual void readSmses(Reader) const = 0;
};

}
