#include "SmsDb.hpp"

namespace ue
{

void SmsDb::addSms(const Sms& sms)
{
    smses.push_back(sms);
}

void SmsDb::readSmses(ISmsDb::Reader reader) const
{
    for (auto& sms : smses)
    {
        if (not reader(sms))
        {
            break;
        }
    }
}

}
