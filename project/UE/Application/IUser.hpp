#pragma once

#include "ISmsDb.hpp"
#include "IUserEventsReceiver.hpp"

namespace ue
{

class IUser
{
public:
    virtual ~IUser() = default;

    virtual void handleAccept(IUserEventsReceiver& eventsReceiver) = 0;

    virtual void setNotConnected() = 0;
    virtual void setConnecting() = 0;
    virtual void setMainMenu() = 0;
    virtual void setComposeSms() = 0;
    virtual void setSmsListView(const ISmsDb&) = 0;
    virtual void setSmsView(const ISmsDb::Sms&) = 0;
};

}
