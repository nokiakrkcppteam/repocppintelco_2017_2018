#include "ConnectingState.hpp"
#include "Application.hpp"

namespace ue
{
std::string ConnectingState::getName() const
{
    return "Connecting";
}

void ConnectingState::handleSib(common::BtsId btsId)
{
    timers.startAttachTimer();
    btsEventsSender.sendAttachRequest(btsId);
    logger.logError(getName(), ": re-attaching on SIB");
}

void ConnectingState::handleAttachAccepted()
{
    timers.stopAttachTimer();
    application.setState(stateFactory.getConnectedState());
    user.setMainMenu();
    logger.logInfo("Attached");
}

void ConnectingState::handleAttachRejected()
{
    timers.stopAttachTimer();
    user.setNotConnected();
    application.setState(stateFactory.getNotConnectedState());
    logger.logError(getName(), ": Attach failed");
}

void ConnectingState::handleAttachTimeout()
{
    user.setNotConnected();
    application.setState(stateFactory.getNotConnectedState());
    logger.logError(getName(), ": Attach timeout");
}

}
