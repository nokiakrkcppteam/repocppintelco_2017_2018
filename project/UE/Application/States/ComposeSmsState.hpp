#pragma once
#include "ConnectedState.hpp"

namespace ue
{

class ComposeSmsState : public ConnectedState
{
public:
    using ConnectedState::ConnectedState;
    std::string getName() const override;

    void handleSmsComposed(const ISmsDb::Sms& sms) override;
    void handleExitCurrentView() override;
};

}
