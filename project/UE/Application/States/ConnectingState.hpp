#pragma once
#include "State.hpp"

namespace ue
{

class ConnectingState : public State
{
public:
    using State::State;
    std::string getName() const override;
    void handleSib(common::BtsId btsId) override;
    void handleAttachAccepted() override;
    void handleAttachRejected() override;
    void handleAttachTimeout() override;
};

}
