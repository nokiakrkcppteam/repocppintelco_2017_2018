#include "ViewSmsesState.hpp"
#include "Application.hpp"

namespace ue
{

std::string ViewSmsesState::getName() const
{
    return "ViewSmses";
}

void ViewSmsesState::handleViewSms(const ISmsDb::Sms &sms)
{
    logger.logDebug(getName(), "Back to main menu");
    application.setState(stateFactory.getViewSmsState());
    user.setSmsView(sms);
}

void ViewSmsesState::handleExitCurrentView()
{
    logger.logDebug(getName(), "Back to main menu");
    application.setState(stateFactory.getConnectedState());
    user.setMainMenu();
}

}
