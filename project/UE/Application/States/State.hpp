#pragma once
#include "Messages/BtsId.hpp"
#include "IBtsEventsSender.hpp"
#include "IEventsReceiver.hpp"
#include "IUser.hpp"
#include "ITimers.hpp"
#include "ISmsDb.hpp"
#include "Logger/ILogger.hpp"
#include <string>
#include <memory>

namespace ue
{

class Application;
class StateFactory;

class State : public IEventsReceiver
{
public:
    
    State(StateFactory& stateFactory,
          Application &application,
          IBtsEventsSender &btsEventsSender,
          IUser& user,
          ITimers &timers,
          ISmsDb& smsDb,
          common::ILogger& logger);
    virtual ~State() = default;

    void handleSib(common::BtsId btsId) override;
    void handleAttachAccepted() override;
    void handleAttachRejected() override;
    void handleAttachTimeout() override;
    void handleSmsReceived(const ISmsDb::Sms& sms) override;

    // IUserEventsReceiver interface
    IUeGui::AcceptClose handleExitUe() override;
    void handleExitCurrentView() override;
    void handleViewSmses() override;
    void handleViewSms(const ISmsDb::Sms &sms) override;
    void handleComposeSms() override;
    void handleSmsComposed(const ISmsDb::Sms &sms) override;


    virtual std::string getName() const = 0;

protected:
    template <typename ...Arg>
    void handleUnexpected(const char* eventName, Arg&& ...eventArg)
    {
        logger.logError("Unexpected \"", eventName, "\" in state: ", getName(),
                        std::forward<Arg>(eventArg)...);
    }

    StateFactory& stateFactory;
    Application& application;
    IBtsEventsSender& btsEventsSender;
    IUser& user;
    ITimers &timers;
    ISmsDb& smsDb;
    common::ILogger& logger;
    std::shared_ptr<State> selfPtr{};

};

}
