#include "ConnectedState.hpp"
#include "Application.hpp"

namespace ue
{

std::string ConnectedState::getName() const
{
    return "Connected";
}

void ConnectedState::handleViewSmses()
{
    logger.logDebug(getName(), "Smses View activated");
    application.setState(stateFactory.getViewSmsesState());
    user.setSmsListView(smsDb);
}

void ConnectedState::handleComposeSms()
{
    logger.logDebug(getName(), "Smses View activated");
    application.setState(stateFactory.getComposeSmsState());
    user.setComposeSms();
}

}
