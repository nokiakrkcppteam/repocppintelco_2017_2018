#include "ComposeSmsState.hpp"
#include "Application.hpp"

namespace ue
{

std::string ComposeSmsState::getName() const
{
    return "ComposeSms";
}

void ComposeSmsState::handleSmsComposed(const ISmsDb::Sms &sms)
{
    logger.logDebug(getName(), "Added sms to: ", sms.to);
    smsDb.addSms(sms);
    btsEventsSender.sendSms(sms);
    handleExitCurrentView();
}

void ComposeSmsState::handleExitCurrentView()
{
    logger.logDebug(getName(), "Back to main menu");
    application.setState(stateFactory.getConnectedState());
    user.setMainMenu();
}

}
