#include "ViewSmsState.hpp"
#include "Application.hpp"

namespace ue
{

std::string ViewSmsState::getName() const
{
    return "ViewSms";
}

void ViewSmsState::handleExitCurrentView()
{
    logger.logDebug(getName(), "Back to view smses");
    application.setState(stateFactory.getViewSmsesState());
    user.setSmsListView(smsDb);
}

}
