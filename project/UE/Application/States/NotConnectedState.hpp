#pragma once
#include "State.hpp"

namespace ue
{

class NotConnectedState : public State {
public:
    using State::State;
    std::string getName() const override;
    void handleSib(common::BtsId btsId) override;
};

}

