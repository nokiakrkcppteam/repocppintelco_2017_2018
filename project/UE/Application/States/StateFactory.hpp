#pragma once
#include <memory>

namespace common
{
class ILogger;
}

namespace ue
{

struct State;
class Application;
class IBtsEventsSender;
class IUser;
class ITimers;
class ISmsDb;

class StateFactory {
public:
    StateFactory(Application &application,
                 IBtsEventsSender &btsEventsSender,
                 IUser& user,
                 ITimers &timers,
                 ISmsDb& smsDb,
                 common::ILogger& logger);

    std::shared_ptr<State> getNotConnectedState();
    std::shared_ptr<State> getConnectingState();
    std::shared_ptr<State> getConnectedState();
    std::shared_ptr<State> getViewSmsState();
    std::shared_ptr<State> getViewSmsesState();
    std::shared_ptr<State> getComposeSmsState();

private:
    std::shared_ptr<State> notConnectedState;
    std::shared_ptr<State> connectingState;
    std::shared_ptr<State> connectedState;
    std::shared_ptr<State> viewSmsState;
    std::shared_ptr<State> viewSmsesState;
    std::shared_ptr<State> composeSmsState;
    Application& application;
    IBtsEventsSender& btsEventsSender;
    IUser& user;
    ITimers &timers;
    ISmsDb& smsDb;
    common::ILogger& logger;
};

}
