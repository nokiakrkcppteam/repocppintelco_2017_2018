#include "StateFactory.hpp"
#include "NotConnectedState.hpp"
#include "ConnectingState.hpp"
#include "ConnectedState.hpp"
#include "ViewSmsState.hpp"
#include "ViewSmsesState.hpp"
#include "ComposeSmsState.hpp"

namespace ue
{

StateFactory::StateFactory(Application &application,
                           IBtsEventsSender &btsEventsSender,
                           IUser &user,
                           ITimers &timers,
                           ISmsDb &smsDb,
                           common::ILogger &logger):
    application(application),
    btsEventsSender(btsEventsSender),
    user(user),
    timers(timers),
    smsDb(smsDb),
    logger(logger)
{}

std::shared_ptr<State> StateFactory::getNotConnectedState()
{
    notConnectedState = std::make_shared<NotConnectedState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return notConnectedState;
}

std::shared_ptr<State> StateFactory::getConnectingState()
{
    connectingState = std::make_shared<ConnectingState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return connectingState;
}

std::shared_ptr<State> StateFactory::getConnectedState()
{
    connectedState = std::make_shared<ConnectedState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return connectedState;
}

std::shared_ptr<State> StateFactory::getViewSmsState()
{
    viewSmsState = std::make_shared<ViewSmsState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return viewSmsState;
}

std::shared_ptr<State> StateFactory::getViewSmsesState()
{
    viewSmsesState = std::make_shared<ViewSmsesState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return viewSmsesState;
}

std::shared_ptr<State> StateFactory::getComposeSmsState()
{
    composeSmsState = std::make_shared<ComposeSmsState>(*this, application,btsEventsSender, user, timers, smsDb, logger);
    return composeSmsState;
}

}

