#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class ViewSmsesState : public ConnectedState
{
public:
    using ConnectedState::ConnectedState;
    std::string getName() const override;

    void handleViewSms(const ISmsDb::Sms& sms) override;
    void handleExitCurrentView() override;
};

}
