#pragma once
#include "ConnectedState.hpp"

namespace ue
{

class ViewSmsState : public ConnectedState
{
public:
    using ConnectedState::ConnectedState;
    std::string getName() const override;

    void handleExitCurrentView() override;
};

}
