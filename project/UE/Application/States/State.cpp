#include "State.hpp"

namespace ue
{

State::State(StateFactory &stateFactory,
             Application &application,
             IBtsEventsSender &btsEventsSender,
             IUser& user,
             ITimers &timers,
             ISmsDb &smsDb,
             common::ILogger &logger):
    stateFactory(stateFactory),
    application(application),
    btsEventsSender(btsEventsSender),
    user(user),
    timers(timers),
    smsDb(smsDb),
    logger(logger)
{}

void State::handleSib(common::BtsId btsId)
{
    handleUnexpected("Sib", " with btsId: ", btsId);
}

void State::handleAttachAccepted()
{
    handleUnexpected("AttachAccepted");
}

void State::handleAttachRejected()
{
    handleUnexpected("AttachRejected");
}

void State::handleAttachTimeout()
{
    handleUnexpected("AttachTimeout");
}

void State::handleSmsReceived(const ISmsDb::Sms &sms)
{
    logger.logDebug(getName(), ": sms received from: ", sms.from);
    smsDb.addSms(sms);
}

IUeGui::AcceptClose State::handleExitUe()
{
    logger.logDebug(getName(), ": allow exit");
    return true;
}

void State::handleExitCurrentView()
{
    handleUnexpected("ExitCurrentView");
}

void State::handleViewSmses()
{
    handleUnexpected("ViewSmses");
}

void State::handleViewSms(const ISmsDb::Sms &sms)
{
    handleUnexpected("ViewSms",  " from ", sms.from, " to ", sms.to);
}

void State::handleComposeSms()
{
    handleUnexpected("ComposeSms");
}

void State::handleSmsComposed(const ISmsDb::Sms &sms)
{
    handleUnexpected("SmsComposed", " to ", sms.to);
}

}
