#include "NotConnectedState.hpp"
#include "Application.hpp"
#include "ConnectingState.hpp"

namespace ue
{

std::string NotConnectedState::getName() const
{
    return "NotConnectedState";
}

void NotConnectedState::handleSib(common::BtsId btsId)
{
    timers.startAttachTimer();
    user.setConnecting();
    btsEventsSender.sendAttachRequest(btsId);
    application.setState(stateFactory.getConnectingState());
}

}
