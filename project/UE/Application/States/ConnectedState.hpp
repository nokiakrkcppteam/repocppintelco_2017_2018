#pragma once
#include "State.hpp"

namespace ue
{

class ConnectedState : public State
{
public:
    using State::State;
    std::string getName() const override;

    void handleViewSmses() override;
    void handleComposeSms() override;
};

}
