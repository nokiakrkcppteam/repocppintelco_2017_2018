#include "User.hpp"
#include "UeGui/IListViewMode.hpp"
#include "UeGui/ISmsComposeMode.hpp"
#include "UeGui/ITextMode.hpp"
#include <sstream>
#include <ctime>
#include <chrono>
#include <iomanip>

namespace ue
{

User::User(common::PhoneNumber phoneNumber,
           IUeGui &ueGui,
           common::ILogger &logger)
    : phoneNumber(phoneNumber),
      ueGui(ueGui),
      logger(logger),
      menuOptions{
          MenuOption{"Sms Compose", "", std::bind(&User::startSmsCompose, this, std::placeholders::_1)},
          MenuOption{"Sms List", "", std::bind(&User::startSmsListView, this, std::placeholders::_1)}
      },
      currentAcceptHandler(std::bind(&User::noActionForAccept, this, std::placeholders::_1))
{
    ueGui.setTitle("UE - " + to_string(phoneNumber));
}

void User::startSmsListView(IUserEventsReceiver &eventsReceiver)
{
    logger.logDebug("SmsListView selected");
    eventsReceiver.handleViewSmses();
}

void User::startSmsCompose(IUserEventsReceiver &eventsReceiver)
{
    logger.logDebug("SmsComposeView selected");
    eventsReceiver.handleComposeSms();
}

void User::handleAccept(IUserEventsReceiver &eventsReceiver)
{
    logger.logDebug("Accept button");
    currentAcceptHandler(eventsReceiver);
}

void User::setNotConnected()
{
    logger.logDebug("Not connecting");
    ueGui.showNotConnected();
}

void User::setConnecting()
{
    logger.logDebug("Connecting");
    ueGui.showConnecting();
}

void User::setMainMenu()
{
    logger.logDebug("Main menu");
    IUeGui::IListViewMode& listMode = ueGui.setListViewMode();
    listMode.clearSelectionList();
    for (auto& option : menuOptions)
    {
        listMode.addSelectionListItem(option.name, option.tooltip);
    }
    currentAcceptHandler = [&](IUserEventsReceiver &eventsReceiver)
    {
        auto selection = listMode.getCurrentItemIndex();
        if (auto notSelected = not selection.first)
        {
            logger.logDebug("NOthing selected - stay in menu");
            return;
        }
        auto index = selection.second;
        try
        {
            menuOptions.at(index).acceptHandler(eventsReceiver);
        }
        catch (std::out_of_range& ex)
        {
            logger.logError("Wrong menu index: ", index);
        }
    };
}

void User::setComposeSms()
{
    logger.logDebug("Compose SMS");
    IUeGui::ISmsComposeMode& smsMode = ueGui.setSmsComposeMode();
    smsMode.clearSmsText();
    currentAcceptHandler = [&](IUserEventsReceiver &eventsReceiver)
    {
        ISmsDb::Sms sms{};
        sms.from = phoneNumber;
        sms.to = smsMode.getPhoneNumber();
        sms.text = smsMode.getSmsText();
        sms.when = std::chrono::system_clock::now();
        eventsReceiver.handleSmsComposed(sms);
    };
}

void User::setSmsListView(const ISmsDb &smsDb)
{
    logger.logDebug("SMS List View");
    IUeGui::IListViewMode& listMode = ueGui.setListViewMode();
    listMode.clearSelectionList();
    Smses currentSmses;
    auto reader = [&](const ISmsDb::Sms& sms)
    {
        currentSmses.push_back(sms);
        listMode.addSelectionListItem(shortSmsText(sms), "");
        return true;
    };
    smsDb.readSmses(reader);

    currentAcceptHandler = [&, smses = std::move(currentSmses)](IUserEventsReceiver &eventsReceiver)
    {
        auto selection = listMode.getCurrentItemIndex();
        if (auto notSelected = not selection.first)
        {
            logger.logDebug("Nothing selected - stay in sms list view");
            return;
        }
        auto index = selection.second;
        try
        {
            auto sms = smses.at(index);
            eventsReceiver.handleViewSms(sms);
        }
        catch (std::out_of_range& ex)
        {
            logger.logError("Bad sms index: ", index);
        }
    };
}

void User::setSmsView(const ISmsDb::Sms &sms)
{
    logger.logDebug("SMS View");
    IUeGui::ITextMode& textMode = ueGui.setViewTextMode();
    textMode.setText(fullSmsText(sms));
    currentAcceptHandler = [&](IUserEventsReceiver &eventsReceiver)
    {
        eventsReceiver.handleExitCurrentView();
    };
}

void User::noActionForAccept(IUserEventsReceiver &eventsReceiver)
{
    logger.logDebug("Ignored Accept in this view");
}

auto User::smsTime(const ISmsDb::Sms &sms) const
{
    std::time_t cTime = std::chrono::system_clock::to_time_t(sms.when);
    return std::put_time(std::localtime(&cTime), "%c %Z");
}

std::string User::shortSmsText(const ISmsDb::Sms &sms) const
{
    std::ostringstream stream;
    if (sms.from != phoneNumber)
    {
        stream << "from: " << sms.from << " ";
    }
    if (sms.to != phoneNumber)
    {
        stream << "to: " << sms.to << " ";
    }

    stream << "[" << smsTime(sms) << "]";

    std::istringstream lines(sms.text);
    std::string firstLine;
    if (std::getline(lines, firstLine))
    {
        stream << "\"" << firstLine.substr(0, 10) << "...";
    }
    return stream.str();
}

std::string User::fullSmsText(const ISmsDb::Sms &sms) const
{
    std::ostringstream stream;
    if (sms.from != phoneNumber)
    {
        stream << "From: " << sms.from << "\n";
    }
    if (sms.to != phoneNumber)
    {
        stream << "To: " << sms.to << "\n";
    }

    stream << "Time: " << smsTime(sms) << "\n";

    stream << "\n" << sms.text;

    return stream.str();
}

}
