#include "SynchronizedApplication.hpp"

namespace ue
{

SynchronizedApplication::SynchronizedApplication(IEventsReceiver &eventsReceiver)
    : eventsReceiver(eventsReceiver)
{}

void SynchronizedApplication::handleSib(common::BtsId btsId)
{
    Lock lock(synchGuard);
    eventsReceiver.handleSib(btsId);
}

void SynchronizedApplication::handleAttachAccepted()
{
    Lock lock(synchGuard);
    eventsReceiver.handleAttachAccepted();
}

void SynchronizedApplication::handleAttachRejected()
{
    Lock lock(synchGuard);
    eventsReceiver.handleAttachRejected();
}

void SynchronizedApplication::handleSmsReceived(const ISmsDb::Sms &sms)
{
    Lock lock(synchGuard);
    eventsReceiver.handleSmsReceived(sms);
}

IUeGui::AcceptClose SynchronizedApplication::handleExitUe()
{
    Lock lock(synchGuard);
    return eventsReceiver.handleExitUe();
}

void SynchronizedApplication::handleExitCurrentView()
{
    Lock lock(synchGuard);
    eventsReceiver.handleExitCurrentView();
}

void SynchronizedApplication::handleViewSmses()
{
    Lock lock(synchGuard);
    eventsReceiver.handleViewSmses();
}

void SynchronizedApplication::handleViewSms(const ISmsDb::Sms &sms)
{
    Lock lock(synchGuard);
    eventsReceiver.handleViewSms(sms);
}

void SynchronizedApplication::handleComposeSms()
{
    Lock lock(synchGuard);
    eventsReceiver.handleComposeSms();
}

void SynchronizedApplication::handleSmsComposed(const ISmsDb::Sms &sms)
{
    Lock lock(synchGuard);
    eventsReceiver.handleSmsComposed(sms);
}

void SynchronizedApplication::handleAttachTimeout()
{
    Lock lock(synchGuard);
    eventsReceiver.handleAttachTimeout();
}

}
