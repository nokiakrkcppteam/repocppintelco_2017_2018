#pragma once

#include "ISmsDb.hpp"
#include "IUeGui.hpp"

namespace ue
{

class IUserEventsReceiver
{
public:
    virtual ~IUserEventsReceiver() = default;

    virtual IUeGui::AcceptClose handleExitUe() = 0;
    virtual void handleExitCurrentView() = 0;

    virtual void handleViewSmses() = 0;
    virtual void handleViewSms(const ISmsDb::Sms& sms) = 0;
    virtual void handleComposeSms() = 0;

    virtual void handleSmsComposed(const ISmsDb::Sms& sms) = 0;
};

}
