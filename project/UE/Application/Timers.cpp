#include "Timers.hpp"

namespace ue
{

Timers::Timers(common::ILogger &logger, ITimeProvider &timeProvider)
    : logger(logger),
      timeProvider(timeProvider),
      timers(),
      attachTimer(timers, "AttachTimer", &ITimeoutsReceiver::handleAttachTimeout, std::chrono::seconds(5), timeProvider, logger)
{}

Timers::Timers(common::ILogger &logger) : Timers(logger, steadyTime)
{}

void Timers::start(ITimeoutsReceiver &receiver)
{
    if (not threadRun.exchange(true))
    {
        logger.logInfo("Timers started");
        timersThread = std::thread([&]{
            logger.logDebug("Timers run - begin");
            while (threadRun)
            {
                auto currentTime = timeProvider.wait();
                logger.logDebug("Next tick at: ", currentTime.time_since_epoch().count());
                step(receiver, currentTime);
            }
            logger.logDebug("Timers run - end");
        });
     }
    else
    {
        logger.logError("Timers already started");
    }
}

void Timers::step(ITimeoutsReceiver &receiver, TimePoint currentTimePoint)
{
    for (auto& timer: timers)
    {
        timer.get().tryExpire(currentTimePoint, receiver);
    }
}

void Timers::stop()
{
    if (threadRun.exchange(false))
    {
        logger.logInfo("Timers stopped");
        timersThread.join();
        logger.logInfo("Timers stopped - success");
    }
    else
    {
        logger.logError("Timers not run or already stopped");
    }
}

void Timers::startAttachTimer()
{
    attachTimer.start();
}

void Timers::stopAttachTimer()
{
    attachTimer.stop();
}

Timers::SteadyTime Timers::steadyTime{std::chrono::milliseconds(10)};

Timers::Timer::Timer(AllTimers &timers,
                     std::string name,
                     Timers::Timeout timeout,
                     Timers::Duration duration,
                     const ITimeProvider &timeProvider,
                     common::ILogger &logger)
    : name(name),
      timeout(timeout),
      expireTimePoint(timeProvider.zeroTime()),
      duration(duration),
      timeProvider(timeProvider),
      logger(logger)
{
    timers.emplace_back(*this);
}

void Timers::Timer::tryExpire(Timers::TimePoint currentTimePoint, ITimeoutsReceiver &receiver)
{
    const auto zeroTime = timeProvider.zeroTime();
    TimePoint expireTimePoint = this->expireTimePoint.exchange(zeroTime);
    if (expireTimePoint != zeroTime)
    {
        if (expireTimePoint <= currentTimePoint)
        {
            (receiver.*timeout)();
            logger.logDebug(name, ": Expired, current: ",
                            currentTimePoint.time_since_epoch().count(),
                            ", expire: ", expireTimePoint.time_since_epoch().count(),
                            ", zero: ", zeroTime.time_since_epoch().count());

        }
        else
        {
            // might already be started again - so do not overwrite non zero
            TimePoint expected = zeroTime;
            bool resetToOldValue = this->expireTimePoint.compare_exchange_strong(expected, expireTimePoint);
            if (not resetToOldValue)
            {
                logger.logDebug(name, ": Warning: timer restarted during expiration.");
            }
        }
    }
}

void Timers::Timer::start()
{
    const auto zeroTime = timeProvider.zeroTime();
    auto expireTimePoint = timeProvider.timeFromNow(duration);
    logger.logDebug(name, " will expire on: ", expireTimePoint.time_since_epoch().count());

    auto oldValue = this->expireTimePoint.exchange(expireTimePoint);
    if (oldValue != zeroTime)
    {
        logger.logError(name, ": already started and not expired - set new expiration Time");
    }
    else
    {
        logger.logDebug(name, ": started");
    }
}

void Timers::Timer::stop()
{
    const auto zeroTime = timeProvider.zeroTime();
    auto oldValue = expireTimePoint.exchange(zeroTime);
    if (oldValue == zeroTime)
    {
        logger.logError(name, ": not run or already expired");
    }
    else
    {
        logger.logDebug(name, ": stopped");
    }
}

Timers::SteadyTime::SteadyTime(Timers::Duration tickDuration)
    : tickDuration(tickDuration)
{}

Timers::TimePoint Timers::SteadyTime::wait()
{
    std::this_thread::sleep_for(tickDuration);
    return std::chrono::steady_clock::now();
}

Timers::TimePoint Timers::SteadyTime::timeFromNow(Timers::Duration duration) const
{
    return std::chrono::steady_clock::now() + duration;
}

Timers::TimePoint Timers::SteadyTime::zeroTime() const
{
    return TimePoint::min();
}

}
