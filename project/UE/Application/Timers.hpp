#pragma once

#include "ITimers.hpp"
#include "ITimeoutsReceiver.hpp"
#include "Logger/ILogger.hpp"

#include <functional>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <string>

namespace ue
{

class Timers : public ITimers
{
public:
    using TimePoint = std::chrono::steady_clock::time_point;
    using Duration = TimePoint::duration;
    class ITimeProvider
    {
    public:
        virtual ~ITimeProvider() = default;
        virtual TimePoint wait() = 0;
        virtual TimePoint timeFromNow(Duration duration) const = 0;
        virtual TimePoint zeroTime() const = 0;
    };

    Timers(common::ILogger& logger);
    Timers(common::ILogger& logger, ITimeProvider &timeProvider); // for UT

    void start(ITimeoutsReceiver &receiver);
    void stop();

    // ITimers interface
    void startAttachTimer() override;
    void stopAttachTimer() override;


private:
    using Timeout = void (ITimeoutsReceiver::*)();
    class Timer;
    using AllTimers = std::vector<std::reference_wrapper<Timer>>;

    class Timer
    {
    public:
        Timer(AllTimers& timers, std::string name, Timeout timeout, Duration duration, const ITimeProvider& timeProvider, common::ILogger& logger);
        void tryExpire(TimePoint currentTimePoint, ITimeoutsReceiver &receiver);
        void start();
        void stop();

    private:
        std::string name;
        Timeout timeout;
        std::atomic<TimePoint> expireTimePoint;
        const Duration duration;
        const ITimeProvider& timeProvider;
        common::ILogger& logger;
    };
    class SteadyTime : public ITimeProvider
    {
    public:
        SteadyTime(Duration tickDuration);
        TimePoint wait() override;
        TimePoint timeFromNow(Duration duration) const override;
        virtual TimePoint zeroTime() const override;
    private:
        Duration tickDuration;
    };

    void step(ITimeoutsReceiver &receiver, TimePoint currentTimePoint);

    static SteadyTime steadyTime;
    common::ILogger& logger;
    ITimeProvider& timeProvider;

    AllTimers timers;
    Timer attachTimer;
    std::atomic_bool threadRun{false};
    std::thread timersThread;
};

}

