#include "Application.hpp"
#include "SynchronizedApplication.hpp"
#include "BtsPort.hpp"
#include "BtsEventsSender.hpp"
#include "User.hpp"
#include "UserPort.hpp"
#include "SmsDb.hpp"
#include "Timers.hpp"
#include "ApplicationEnvironmentFactory.hpp"

int main(int argc, char* argv[])
{
    using namespace ue;

    auto appEnv = ue::createApplicationEnvironment(argc, argv);
    auto& logger = appEnv->getLogger();
    auto& transportToBts = appEnv->getTransportToBts();
    auto& ueGui = appEnv->getUeGui();
    auto phoneNumber = appEnv->getMyPhoneNumber();

    Timers timers(logger);
    BtsEventsSender btsEventsSender(phoneNumber, transportToBts, logger);
    SmsDb smsDb{};
    User user(phoneNumber, ueGui, logger);
    Application app(phoneNumber, btsEventsSender, user, timers, smsDb, logger);
    SynchronizedApplication syncApp(app);
    BtsPort btsPort(syncApp, phoneNumber, transportToBts, logger);
    UserPort userPort(syncApp, user, ueGui, logger);
    logger.logInfo("Started: ", phoneNumber);
    timers.start(syncApp);
    appEnv->startMessageLoop();
    timers.stop();
    logger.logInfo("Stopped: ", phoneNumber);
}

